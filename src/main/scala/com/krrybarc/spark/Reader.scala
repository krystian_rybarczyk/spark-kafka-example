package com.krrybarc.spark

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.{KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

object Reader extends App {

  val kafkaBrokers = "192.168.88.235:32775"

  val conf = new SparkConf()
    .setAppName("toyProject")
    .setMaster("local")

  val sc = new SparkContext(conf)

  val streamingContext = new StreamingContext(sc, Seconds(10))

  val kafkaParams = Map[String, Object](
    "bootstrap.servers" -> kafkaBrokers,
    "key.deserializer" -> classOf[StringDeserializer],
    "value.deserializer" -> classOf[StringDeserializer],
    "group.id" -> "topic",
    "auto.offset.reset" -> "earliest",
    "enable.auto.commit" -> (true: java.lang.Boolean)
  )

  val topics = Array("topic")

  val stream = KafkaUtils.createDirectStream[String, String](
    streamingContext,
    LocationStrategies.PreferConsistent,
    Subscribe[String, String](topics, kafkaParams)
  )

  stream
    .map(record => record.value())
    .map(_.toUpperCase)
    .saveAsTextFiles("/tmp/output")

  streamingContext.start()
  streamingContext.awaitTermination()
}
