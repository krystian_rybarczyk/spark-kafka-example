package com.krrybarc.spark

import org.apache.spark.{SparkConf, SparkContext}

object FileReader extends App {

  val inputFile = args(0)
  val conf = new SparkConf().setAppName(" wordCount").setMaster("local")
  val sc = new SparkContext(conf)
  val input = sc.textFile(inputFile)
  val words = input.flatMap(line => line.split(" ")).map(_.replaceAll("[^A-Za-z0-9]", ""))
  val counts = words.map(word =>(word, 1)).reduceByKey { case (x, y) => x +y}
  counts.saveAsTextFile("/tmp/file-result")

}
